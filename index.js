/*
Escriba una función que dados un array de enteros, distintos todos entre sí, y un número que representa
la suma objetivo, devuelva un arreglo con el par de números cuya suma sea igual al valor de suma objetivo.
Si no se encuentra igualdad, retorne un arreglo vacío. Imprima el resultado en pantalla
*/

function twoNumberSum(array, targetSum){
    var array_temp = []
    var bol = 0
    for (let i = 0; i < array.length; i++){
        for (let j = i+1; j < array.length; j++){
            if ((array[i] + array[j] === targetSum) && bol === 0){
                array_temp.push(array[i], array[j])
                bol = 1
            }
        }
    }
    return array_temp
}

function imprimir(result){
    if (result.length === 0){
        console.log("No hay coincidencias")
    }
    else{
        console.log("El array resultante es: " + "[" + result + "]")
    }
}

const array = [3, 5, -4, 8, 11, 1, -1, 6]
const targetSum = 10
const array1 = [3, 5, -4, 8, 11, 1, -2, 6]
const targetSum1 = 10
const result = twoNumberSum(array, targetSum)
const result1 = twoNumberSum(array1, targetSum1)
imprimir (result)
imprimir (result1)